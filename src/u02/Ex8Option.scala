package u02
/**1
import u02.Optionals.Option
import u02.Optionals.Option.{None, Some, flatMap, getOrElse}

**/
object Ex8Option extends App {

  sealed trait Option[A] // An Optional data type
  object Option {
    case class None[A]() extends Option[A]
    case class Some[A](a: A) extends Option[A]

    def isEmpty[A](opt: Option[A]): Boolean = opt match {
      case None() => true
      case _ => false
    }

    def getOrElse[A, B >: A](opt: Option[A], orElse: B): B = opt match {
      case Some(a) => a
      case _ => orElse
    }

    def flatMap[A,B](opt: Option[A])(f:A => Option[B]): Option[B] = opt match {
      case Some(a) => f(a)
      case _ => None()
    }

    def filter[A](opt: Option[A])(f: A=>Boolean): Option[A] = opt match {
      case Some(a) if f(a) => Some(a)
      case _ => None()
    }

    def map[A, B](opt: Option[A])(f: A=>B): Option[B] = opt match {
      case Some(a) => Some(f(a))
      case _ => None()
    }

//    def bothEmpty[A](opt: Option[A], opt2: Option[A]): Boolean = ((isEmpty[A](opt)) && (isEmpty[A](opt2)))

    def map2[A](opt: Option[A], opt2: Option[A]): Option[A] = (isEmpty[A](opt)) && (isEmpty[A](opt2)) match {
      case y:Boolean if !y => None()
      //case _ if true => Some(1)
    }

    def map3[A](opt: Option[A], opt2: Option[A]): Option[A] = (opt, opt2) match {
      case (Some(opt),Some(opt2)) =>Some(opt)
      case _ if true => None()
    }

    /**
     * case (Some(opt),Some(opt2)) =>Some( match {
     * case Some() =>
     * case None =>
     * })
     * case _ if true => None()
     */

  }

  import Option._
  println(filter(Some(5))(_>2))
  println(filter(Some(5))(_>17))
  println(filter(Some(5))(_==5))

  println(map(Some(5))(_>2))
  println(map(None[Int])(_>2))

  println("MAP2" + map2(Some(5),None()))
  println("MAP3 " + map3(Some(5),None()))
  println("MAP3 " + map3(Some(5),Some(2)))



}
