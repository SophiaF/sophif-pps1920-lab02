package u02

object Ex7GeometricShapes extends App {


  sealed trait Shape
  object Shape {

    case class Rectangle(width: Double, height: Double) extends Shape

    case class Circle (radius: Double) extends Shape

    case class Square (side: Double) extends Shape

    def perimeter (shape: Shape): Double = shape match {
      case Rectangle(w,h) => w*2 + h*2
      case Circle(r) => (2 * 3.14 * r)
      case Square(l) => l*4
    }

    def area (shape: Shape): Double = shape match {
      case Rectangle(w,h) => w*h
      case Circle(r) => 3.14 * r * r
      case Square(l) => l*l
    }

  }

}
