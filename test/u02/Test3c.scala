package u02
import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.FirstClass.negGen

class Test3c {

  val empty: String => Boolean = _=="" // predicate on string
  val notEmpty = negGen[String](empty) // which type of notEmpty?

  @Test def testNotEmpty(): Unit ={
    assertEquals(true, notEmpty("foo"))
    assertEquals(true, notEmpty("foo") && (!notEmpty("")))
  }

  @Test def testEmpty(): Unit ={
    assertEquals(false, notEmpty("") )
  }

}
