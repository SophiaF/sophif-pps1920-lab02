package u02

object FirstClass extends App {
  //3A)
  def parity(x:Int): String = x%2 match {
    case 0 => "even"
    case _ => "odd"
  }

  val parity_val: Int => String = {
    case e if e%2==0 => "even"
    case _ => "odd"
  }

  //3B)
  val neg_val: (String=>Boolean) => (String=> Boolean) = (p) => (s => !(p(s)))
  def neg(e: String => Boolean) : (String=> Boolean) = (s:String) => !e(s)

  val empty: String => Boolean = _=="" // predicate on string

  val notEmpty = neg_val(empty) // which type of notEmpty?
  notEmpty("foo")// true notEmpty("") // false
  notEmpty("foo") && (!notEmpty("")) // true.. a comprehensive test

  //3C)
  def negGen[A](e: A => Boolean) : (A=> Boolean) = (a: A) => !e(a)

  //4
  val p1: Int => Int => Int => Boolean = x => y => z => ((x <= y) && (y <= z))
  println(p1(1)(3)(10))
  println(p1(7)(3)(10))

  val p2 = (x: Int, y: Int, z:Int) => ((x <= y) && (y <= z))
  println(p2(2,3,5))
  println(p2(7,3,10))

  //Curriyng
  def p3 (x: Int)(y: Int)(z: Int): Boolean = ((x <= y) && (y <= z))
  println(p3(2)(3)(5))
  println(p3(7)(3)(10))

  //No Currying
  def p4 (x: Int, y: Int, z: Int): Boolean = ((x<= y) && (y<= z))
  println(p4(2,3,5))
  println(p4(7,3,10))

  //5
  /** Ne esiste una preferibile in modo assoluto?**/
  //def compose[A](f: A => A, g: A => A)(a: A):  A = f(g(a))
  //def compose[A](f: A => A, g: A => A, a: A): A = f(g(a))
  def compose[A](f: A => A, g: A => A): A => A = a => f(g(a))
  println(compose[Int](_-1, _*2)(5))
  //println(compose[Int](_-1, _*2, 5))

  //6
  /***Non è tail perchè si fa la somma come ultima espressione prima del return***/
  def fib(n:Int): Int = n match {
    case 0 => 0
    case 1 => 1
    case _ => fib(n-1) + fib(n-2)
  }
  println(fib(4))

}
