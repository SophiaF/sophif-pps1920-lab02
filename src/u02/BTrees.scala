package u02

import Option._
import u02.Optionals.Option
import u02.Optionals.Option._


object BTrees extends App {

  // A custom and generic binary tree of elements of type A
  sealed trait Tree[A]
  object Tree {
    case class Leaf[A](value: A) extends Tree[A]
    case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

    def size[A](t: Tree[A]): Int = t match {
      case Branch(l, r) => size(l) + size(r)
      case _ => 1
    }

    def find[A](t: Tree[A], elem: A): Boolean = t match {
      case Branch(l, r) => find(l, elem) || find (r,elem)
      case Leaf(e) => e==elem
    }

    def count[A](t: Tree[A], elem: A): Int = t match {
      case Branch(l, r) => count(l, elem) + count(r,elem)
      case Leaf(e) if (e==elem) => 1
      case _ => 0
    }

    /* def getCourse2(lp: List[Person]): List[String] = {
    flatMap(lp)(p => p match {
      case Teacher(n,c) =>Cons(c, Nil())
      case _ => Nil()
    })
  }*/

    def operationTree[A, B](t: Tree[A])(operation: (B, B)=> B)(f: A=>B): B = t match {
      case Branch(l, r) =>operation(operationTree(l)(operation)(f), operationTree(r)(operation)(f))
      case Leaf(e) => f(e)
    }

    /*
    def negate[A](opt: Option[A]): Option[A] = opt match {
      case Some[Int](a) =>
      case _ => None()
    }*/
  }

  import Tree._
  val tree = Branch(Branch(Leaf(1),Leaf(2)),Leaf(1))

  println("Size: " + operationTree[Int, Int](tree)((f, s)=> f+s)(l => 1))
  println("Find: " + operationTree[Int, Boolean](tree)((f, s)=> f || s)(l => (l==1)))
  println("Count: " + operationTree[Int, Int](tree)((f, s)=> f+s)(l => l match {
    case _ if l==1 => 1
    case _ => 0
  } ))

  /* println("Neg" + negate[Boolean](Some(true)))
   println("Neg2" + negate[Boolean](Some(false)))
   println("Neg3" + negate[Int](Some(2)))
   println("Neg4" + negate(None()))*/
}

