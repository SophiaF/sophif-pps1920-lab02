package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.Ex7GeometricShapes.Shape.{Circle, Rectangle, Square}


class TestShape {

  import Ex7GeometricShapes.Shape._

  val rectangle = Rectangle(2, 7.5)
  val square = Square(2.5)
  val circle = Circle(2) //Ho avuto problemi nel numero di cifre significative

  @Test def testPerimeters(): Unit ={
    assertEquals(19.0, perimeter(rectangle))
    assertEquals(10.0, perimeter(square))
    assertEquals(12.56, perimeter(circle))
  }

  @Test def testAreas(): Unit = {
    assertEquals(15, area(rectangle))
    assertEquals(6.25, area(square))
    assertEquals(12.56, area(circle))
  }



}
